global TIRETTE_DISABLED

from enum import Enum

try:
    import RPi.GPIO as GPIO
    TIRETTE_DISABLED = False
except (RuntimeError, ImportError) :
    print("Failed to access GPIO. Tirette is in default mode.")
    TIRETTE_DISABLED = True




class TiretteState(Enum):
    ARMED = 1
    DISARMED = 0

class Tirette:
    def __init__(self, pin: int):
        Tirette.tirette = self

        if TIRETTE_DISABLED:
            return 
        
        self._pin = pin

        GPIO.setwarnings(False)
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(self._pin, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)


    def get_state(self, default: TiretteState = TiretteState.DISARMED) -> TiretteState :
        """
        Returns the state of the tirette.
        ARMED if plugged in, DISARMED IF not.
        default is used when GPIO cannot be imported. (ig: debug)
        Blocking call
        """

        if TIRETTE_DISABLED:
            return default
        
        if GPIO.input(self._pin) == GPIO.LOW:
            return TiretteState.DISARMED
        
        return TiretteState.ARMED