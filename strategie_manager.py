from abc import ABC, abstractmethod, abstractstaticmethod
from typing import Iterable, Type, Dict, Callable, List, Tuple
from threading import Thread, Event
from time import time, sleep
import logging
from tkinter import Widget
from enum import Enum
from numbers import Number
from .tirette import Tirette, TiretteState

class TeamColor(Enum):
    BLEU = 0
    VERT = 1

class Strategie(ABC):
    @abstractmethod
    def __init__(self, team_color: TeamColor, aruco: Dict[str, int]) -> None:
        """
        This method is called when the strategie is choosen,
        it may automatically move the robot to the desired starting point.
        """
        pass

    @abstractmethod
    def start(self) -> None:
        """
        This method is used to start the strategie.
        It should be called when the "tirette" is pulled.
        """
        pass

    @abstractmethod
    def abort(self) -> None:
        """
        This method is used to stop the strategie, while it is running or at 100 seconds.
        """
        pass

    @abstractmethod
    def score(self) -> int:
        """
        This method is used for score estimation.
        """
        pass

    def setup(self) -> None:
        pass


# class ListedStrategie(Strategie, ABC):
#     def __init__(self, team_color: TeamColor, aruco: Dict[str, int]) -> None:
#         pass

#     @abstractstaticmethod
#     def _get_strat_commands() -> Iterable[Command]:
#         pass

#     def update(self):
#         # lidar.check()

#         # superviseur.check()

#         # if (self.current_command == deplacement):
#         #     pass
#         # elif (self.current_command == trier_les_couches):
#         #     pass
#         # else:
#         #     raise ValueError(f"Unknown command \"{self.current_command}\"")

#         # if (self.current_command.ended and len(self.commands) > 0):
#         #     self.current_command = self.commands.pop()
#         # elif (self.current_command.ended and len(self.commands) == 0):
#         #     self.abort()
#         pass

#     def _main_loop(self):
#         while len(self.commands) > 0 and not self._abort:
#             self.update()

#     def start(self):
#         self.current_command = self.commands.pop()
#         self._main_loop_thread = Thread(target=self._main_loop)
#         self._abort = False
#         self._main_loop_thread.start()

#     def abort(self):
#         if self._main_loop_thread is not None:
#             self._abort = True
#             self._main_loop_thread.join()
#         StmManager.stm_manager.stop()

#     def score(self) -> int:
#         pass

class StrategieManager:
    def __init__(self, strats_dict: Dict[str, Type[Strategie]]) -> None:
        StrategieManager.strategie_manager = self
        StrategieManager.strats_dict: Dict[str, Type[Strategie]] = strats_dict
        
        self._current_strat = None
        self._strat: Type[Strategie] = None
        self._team_color: TeamColor = None
        self._aruco: Dict[str, int] = None

        self._logs: List[str] = []
        self._state: str = ""
        self._log_callback: Callable[[str], None] = None
        self._state_callback: Callable[[str], None] = None

        self._watcher_thread: Thread = None



    def is_strat_running(self) -> bool:
        return self._current_strat is not None

    def init_strat(self, strat: Type[Strategie], team_color: TeamColor, aruco: Dict[str, int]):
        # Instiantiating strat
        logging.info("Gonna instanciate strategie")
        self._current_strat = strat(team_color, aruco)
        self._watcher_thread = Thread(target=self._update_watch)
        self._watcher_thread.start()
        logging.info("Strat thread started")

    def _startup_process(self):
        #Wait for the tirette to be plugged in
        self.set_state("Armer la tirette")
        while Tirette.tirette.get_state(default=TiretteState.ARMED) is not TiretteState.ARMED:
            sleep(0.1)

        #Wait for the tirette to be pulled out
        self.set_state("Tirette armée")
        while Tirette.tirette.get_state(default=TiretteState.DISARMED) is TiretteState.ARMED:
            sleep(0.1)
    
    def _update_watch(self) -> None:
        self._current_strat.setup()
        self._startup_process()

        self._current_strat.start()
        self._start_time = time()

        while self.is_strat_running() :
            elapsed_time_second: Number = time() - self._start_time
            if elapsed_time_second >= 100:
                score: int = self._current_strat.score()
                self._current_strat.abort()
                self.set_state(f"Temps écoule, {score} points marqués")
                break
            else:
                self.set_state(f"{elapsed_time_second:03.0f}/100 secondes écoulées", False)
                sleep(.5)

    def _update_fallback(self) -> None:
        pass
    
    def abort_strat(self):
        logging.info("Aborting strat")
        self._current_strat.abort()
        self._current_strat = None
        self._logs = []
        self._state = ""
        self.set_state("Strategie aborted")

    def set_log_callback(self, callback: Callable[[str], None]) -> None:
        self._log_callback = callback
        if callback is not None:
            for log in self._logs:
                callback(log)
    def set_state_callback(self, callback: Callable[[str], None]) -> None:
        self._state_callback = callback
        if callback is not None:
            callback(self._state)

    def set_state(self, state: str, log_state: bool = True) -> None:
        if log_state:
            logging.info(f"IHM state setted to: \"{state}\"")
        self._state = state
        if self._state_callback is not None:
            self._state_callback(state)
    def log(self, log: str) -> None:
        self._logs.append(log)
        if self._log_callback is not None:
            self._log_callback(log)
